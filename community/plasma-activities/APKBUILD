# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-activities
pkgver=6.1.2
pkgrel=0
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
pkgdesc="Core components for the KDE's Activities"
url="https://invent.kde.org/plasma/plasma-activities.git"
license="GPL-2.0-or-later AND LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="qt6-qtbase-sqlite"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	kwindowsystem-dev
	qt6-qtdeclarative-dev
	"
makedepends="$depends_dev
	boost-dev
	doxygen
	extra-cmake-modules
	qt6-qttools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc"
_repo_url="https://invent.kde.org/plasma/plasma-activities.git"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-activities-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c970088af816de23330d07a98bcdaf372cff2eae8efbfe56d9043c19981cb71af6a1f44a84b361b6fb3eb5657a3a3b8ef9f428884a6083f1ea52f4e85d029472  plasma-activities-6.1.2.tar.xz
"
